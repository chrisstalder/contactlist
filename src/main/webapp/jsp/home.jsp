<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form" %> <!--Form validation -->
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title>Hello Controller Page</title>
        <!-- Bootstrap core CSS -->
        <link href="${pageContext.request.contextPath}/css/bootstrap.min.css" rel="stylesheet">

        <!-- SWC Icon -->
        <link rel="shortcut icon" href="${pageContext.request.contextPath}/img/icon.png">

    </head>
    <body>
        <div class="container">
            <h1>Contact List</h1>
            <hr/>
            <div class="navbar">
                <ul class="nav nav-tabs">
                    <li role="presentation"><a href="${pageContext.request.contextPath}">Home</a></li>
                </ul>    
            </div>

            <div class="row">

                <div class="col-md-6">
                    <table class="table table-hover" id="contact-table">
                        <tr>
                            <th>First Name</th>
                            <th>Last Name</th>
                            <th>Edit</th>
                            <th>Delete</th>
                        </tr>
                        <c:forEach items="${contacts}" var="contact">
                            <tr id="contact-row-${contact.id}">
                                <td><a data-contact-id="${contact.id}" data-toggle="modal" data-target="#showContactModal">${contact.firstName}</a></td>
                                <td>${contact.lastName}</td>
                                <td><a data-contact-id="${contact.id}" data-toggle="modal" data-target="#editContactModal">Edit</a></td>
                                <td><a data-contact-id="${contact.id}" class="delete-link">Delete</a></td>
                            </tr>
                        </c:forEach>
                    </table>
                </div>
                <div class="col-md-6">
                    <form method="POST" class="form-horizontal">
                        <div class="form-group">
                            <label for="first-name" class="col-md-4 control-label">First: </label>
                            <div class="col-md-8">
                                <input type="text" id="firstName-input" class="form-control"></input>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="last-name" class="col-md-4 control-label">Last: </label>
                            <div class="col-md-8">
                                <input type="text" id="lastName-input" class="form-control"></input>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="company" class="col-md-4 control-label">Company: </label>
                            <div class="col-md-8">
                                <input type="text" id="company-input" class="form-control"></input>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="email" class="col-md-4 control-label"> Email: </label>
                            <div class="col-md-8">
                                <input type="text" id="email-input" class="form-control"></input>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="phone" class="col-md-4 control-label"> Phone:</label>
                            <div class="col-md-8">
                                <input type="text" id="phone-input" class="form-control"></input>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="phone" class="col-md-4 control-label"> Last Contacted: </label>
                            <div class="col-md-8">
                                <input type="text" id="lastContacted-input" class="form-control"></input>

                            </div>
                        </div>
                        <div id="add-contact-validation-errors" class="pull-right">
                            
                        </div>
                        <input id="create-submit" type="submit" class="btn btn-default center-block"/>

                    </form>
                </div>
            </div>    

        </div>

        <div id="showContactModal" class="modal fade" tabindex="-1" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">Contact Details</h4>
                    </div>
                    <div class="modal-body">

                        <table class="table table-bordered" id="show-contact-table">

                            <tr>
                                <th>First Name:</th>
                                <td id="contact-first-name"></td>
                            </tr>
                            <tr>
                                <th>Last Name:</th>
                                <td id="contact-last-name"></td>
                            </tr>
                            <tr>
                                <th>Company:</th>
                                <td id="contact-company"></td>
                            </tr>
                            <tr>
                                <th>Email:</th>
                                <td id="contact-email"></td>
                            </tr>
                            <tr>
                                <th>Phone:</th>
                                <td id="contact-phone"></td>
                            </tr>

                        </table>



                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->

        <div id="editContactModal" class="modal fade" tabindex="-1" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">Edit Contact Details</h4>
                    </div>
                    <div class="modal-body">

                        <table class="table table-bordered">
                            <input type="hidden" id="edit-id"/>
                            <tr>
                                <th>First Name:</th>
                                <td>
                                    <input type="text" id="edit-contact-first-name"/>
                                </td>
                            </tr>
                            <tr>
                                <th>Last Name:</th>
                                <td>
                                    <input type="text" id="edit-contact-last-name"/>
                                </td>
                            </tr>
                            <tr>
                                <th>Company:</th>
                                <td>
                                    <input type="text" id="edit-contact-company"/>
                                </td> 
                            </tr>
                            <tr>
                                <th>Email:</th>
                                <td>
                                    <input type="text" id="edit-contact-email"/>
                                </td>
                            </tr>
                            <tr>
                                <th>Phone:</th>
                                <td>
                                    <input type="text" id="edit-contact-phone"/>
                                </td> 
                            </tr>

                        </table>



                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="button" class="btn btn-primary" id="edit-contact-button">Save changes</button>
                    </div>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->

        <script>
            var contextRoot = "${pageContext.request.contextPath}";
        </script>

        <!-- Placed at the end of the document so the pages load faster -->
        <script src="${pageContext.request.contextPath}/js/jquery-1.11.1.min.js"></script>
        <script src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>
        <script src="${pageContext.request.contextPath}/js/app.js"></script>
    </body>
</html>

